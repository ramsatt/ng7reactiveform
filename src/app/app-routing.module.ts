import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './form/home/home.component';
import {BasicComponent} from './form/basic/basic.component';
import {NestedComponent} from './form/nested/nested.component';
import {DynamicComponent} from './form/dynamic/dynamic.component';
import {ValidationComponent} from './form/validation/validation.component';
import {ControlvalidationComponent} from './form/controlvalidation/controlvalidation.component';
import {SubmitComponent} from './form/submit/submit.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent
},
  {
    path: 'basic',
    component: BasicComponent
  },
  {
    path: 'nested',
    component: NestedComponent
  },
  {
    path: 'dynamic',
    component: DynamicComponent
  },
  {
    path: 'validation',
    component: ValidationComponent
  },
  {
    path: 'cvalidation',
    component: ControlvalidationComponent
  },
  {
    path: 'submit',
    component: SubmitComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
