import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {
  MatDividerModule,
  MatIconModule,
  MatListModule,
  MatToolbarModule,
  MatInputModule,
  MatButtonModule,
  MatGridListModule,
  MatRadioModule, MatCheckboxModule, MatChipsModule
} from '@angular/material';
import { BasicComponent } from './basic/basic.component';
import { NestedComponent } from './nested/nested.component';
import { DynamicComponent } from './dynamic/dynamic.component';
import { ValidationComponent } from './validation/validation.component';
import { ControlvalidationComponent } from './controlvalidation/controlvalidation.component';
import { SubmitComponent } from './submit/submit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
@NgModule({
  declarations: [
    HomeComponent,
    BasicComponent,
    NestedComponent,
    DynamicComponent,
    ValidationComponent,
    ControlvalidationComponent,
    SubmitComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    MatInputModule,
    MatButtonModule,
    MatGridListModule,
    MatRadioModule,
    MatCheckboxModule,
    MatChipsModule
  ]
})
export class FormModule { }
