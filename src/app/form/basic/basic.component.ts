import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit {
  myForm: FormGroup;
  constructor(private fb: FormBuilder) {
    this.createForm();
    this.myForm.valueChanges.subscribe(console.log);
  }

  ngOnInit() {
  }

  createForm() {
    this.myForm = this.fb.group({
      'name': [''],
      'email': ['']
    });
  }

}
