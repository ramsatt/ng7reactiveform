import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  formsType: any;

  constructor(private router: Router) { }

  ngOnInit() {
    this.formsType = [
      {icon: 'looks_one', name: 'Basic Form', link: 'basic'},
      {icon: 'looks_two', name: 'Nested Form', link: 'nested'},
      {icon: 'looks_3', name: 'Dynamic Form Field', link: 'dynamic'},
      {icon: 'looks_4', name: 'Form Validation', link: 'validation'},
      {icon: 'looks_5', name: 'Playing with controls', link: 'cvalidation'},
      {icon: 'looks_6', name: 'Form Submit', link: 'submit'}
      ];
  }

  navigation(link) {
    this.router.navigate([link]);
  }

}
