import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.scss']
})
export class DynamicComponent implements OnInit {
  myForm: FormGroup;
  constructor(private fb: FormBuilder) {
    this.createForm();
    this.myForm.valueChanges.subscribe(console.log);
  }

  ngOnInit() {
  }

  createForm() {
    this.myForm = this.fb.group({
      name: [''],
      email: [''],
      address: this.fb.array([])
    });
  }

  get AddressForm() {
    return this.myForm.get('address') as FormArray;
  }

  addAddress() {
    const addressGroup = this.fb.group({
      street: [''],
      city: [''],
      state: ['']
    });
    this.AddressForm.push(addressGroup);
  }

  deleteAddress(i) {
    this.AddressForm.removeAt(i);
  }

}
