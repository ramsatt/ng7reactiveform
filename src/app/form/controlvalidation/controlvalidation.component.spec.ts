import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlvalidationComponent } from './controlvalidation.component';

describe('ControlvalidationComponent', () => {
  let component: ControlvalidationComponent;
  let fixture: ComponentFixture<ControlvalidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlvalidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlvalidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
