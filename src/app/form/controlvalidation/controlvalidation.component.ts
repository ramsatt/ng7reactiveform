import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-controlvalidation',
  templateUrl: './controlvalidation.component.html',
  styleUrls: ['./controlvalidation.component.scss']
})
export class ControlvalidationComponent implements OnInit {
  myForm: FormGroup;
  constructor(private fb: FormBuilder) {
    this.createForm();
    this.myForm.valueChanges.subscribe(data => {
      console.log(data);
    });
  }

  ngOnInit() {
  }

  createForm() {
    this.myForm = this.fb.group({
      name: [''],
      laptop: [false],
      laptopName: [{value: '', disabled: true}]
    });
  }

  setName() {
    this.myForm.controls['name'].setValue('Sathish');
  }

  resetForm() {
    this.myForm.reset();
  }

  handleControl(event) {
    if (event.checked) {
      this.myForm.controls['laptopName'].enable();
      this.myForm.controls['laptopName'].setValidators([Validators.required]);
      this.myForm.controls['laptopName'].updateValueAndValidity();
    } else {
      this.myForm.controls['laptopName'].disable();
      this.myForm.controls['laptopName'].setValidators([]);
      this.myForm.controls['laptopName'].updateValueAndValidity();
    }
  }

}
