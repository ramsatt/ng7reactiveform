import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-nested',
  templateUrl: './nested.component.html',
  styleUrls: ['./nested.component.scss']
})
export class NestedComponent implements OnInit {
  myForm: FormGroup;
  constructor(private fb: FormBuilder) {
    this.createForm();
    this.myForm.valueChanges.subscribe(console.log);
  }

  ngOnInit() {
  }

  createForm() {
    /*Sub Form Group for Address*/
    const addressGroup = this.fb.group({
      street: [''],
      city: [''],
      state: ['']
    });
    /*Main Form Group*/
    this.myForm = this.fb.group({
      name: [''],
      email: [''],
      address: addressGroup
    });
  }

}
